﻿Preisänderung
=============

Um eine Preisänderung durchzuführen, wählen Sie zunächst Ihre
Filiale aus und stellen Sie den gewünschten Preis ein. Neben der
Schaltfläche zum Scannen befindet sich eine weitere kleine Schaltfläche,
die es erlaubt umzuschalten, ob ein einzelner ``1`` oder mehrere Artikel
in Folge ``n`` gescannt werden sollen.

.. image:: media/n.jpg
   :width: 200

Nach Abschluss des Scanvorgangs erscheinen alle Artikel zur Kontrolle in
der Liste. Hier ist es möglich den Preis noch einmal zu korrigieren,
indem man auf die blaue Preisanzeige tippt.

Unterhalb der Liste finden sich Informationen über den auszuführenden
Vorgang.
Sind alle Eingaben korrekt kann die Preisänderung durch den Klick auf
**``Buchen``** ausgeführt werden.

So gebuchte Preisänderungen können in der prohandel-Warenwirtschaft im
Programm ``Preisänderung`` verbucht werden. 
Benutzen Sie dafür einfach den Button ``Vorerfasste Preisänderungen``.

.. image:: media/image31.png
   :width: 200


Sie können nicht nur absolute, sondern auch eine prozentuelle
Preisänderung durchführen.
Dafür klicken Sie auf die zwei Pfeile in der Leiste **``Neuer Preis``** 
und geben Ihren Prozentsatz an. Abschließend buchen Sie die  
Preisänderung.

.. image:: media/AEND.jpg
   :width: 200


.. image:: media/bs.jpg
   :width: 200

