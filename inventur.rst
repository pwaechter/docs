﻿Inventur 
========

.. image:: media/image28.png
   :width: 200
   
Zu Beginn der Inventurerfassung muss eine in der prohandel-Warenwirtschaft 
eröffnete Inventur ausgewählt werden. Ist das geschehen
können vorhandene Fächer editiert oder neue Fächer angelegt werden.

Editierfunktionen wie die Freigabe eines zugewiesenen Fachs, oder das
Löschen eines Fachs können über eine Wisch-Geste auf dem entsprechenden
Fach erreicht werden. Alternativ können Sie ein Fach neu zuweisen.

.. image:: media/zuweisung.jpg
   :width: 200

Unterstützte Arbeitsweisen
--------------------------

Die Inventur kann effizienter erfolgen, indem ein zuständiger Mitarbeiter 
eine Vorerfassung vor dem Inventurbeginn tätigt.
Er vergibt die Fachnummer und die Sollmenge für jeden Artikel. 
Während der Aufnahmeinventur sollen dann nur die schon angelegten 
Fächer geöffnet und die Artikel gescannt werden.


.. note:: Wenn es während der Inventur die Anzahl der gescannten Artikeln mit der Vorzählung nicht übereinstimmen würde, bekommen Sie die Meldung und können  sich dann entscheiden, ob die Daten übermittelt sein sollen.

.. image:: media/AD.jpg
   :width: 200

Als eine andere Option bittet sich eine direkte Erfassung,
wodurch jeder Mitarbeiter (während der Aufnahmeinventur) das Fach anlegt 
und sich selbst zuweist. Das Fach wird direkt geöffnet und 
die Artikeln können dann sofort gescannt und verbucht werden.


Fach anlegen
------------
Über das Plus-Symbol, in der oberen rechten Ecke des Bildschirms, können
neue Fächer angelegt werden.

.. image:: media/mir_zuweisen.jpg
   :width: 200

   
Zur Anlage eines Fachs muss eine Fachnummer eingegeben werden,
optional können Sie die Fachnummer per Scanner erfassen. 
Des Weiteren ist es möglich eine Sollmenge für jedes Fach
zu definieren, wobei dies eine optionale Angabe ist.


Artikel erfassen
----------------

.. image:: media/image30.png
   :width: 200

Artikel werden über den angezeigten Scanner erfasst und in
der Liste angezeigt. Gescannte Artikel werden auf dem Gerät zwischengespeichert 
und eine Fach-Erfassung kann somit jederzeit unterbrochen werden.

.. note:: Beim Tippen auf das Symbol mit den drei Punkten in der oberen rechten Ecke des Displays, können jederzeit die Scannereinstellungen vorgenommen werden.

Über die Funktion **``Buchen``** oder **``Wischen um zu buchen``** kann das Fach
geschlossen werden. 

Wenn Sie nochmal auf das erfasste Fach klicken, erscheint die Meldung 
``Die Erfasung des ausgewählten Faches ist bereits abgeschlossen
und wurde an den Server übertragen``.

Eine Nacherfassung im ausgewählten Fach kann im Nachhinein erfolgen. 
Dafür klicken Sie auf das jeweilige Fach
und wählen **``Nacherfassung starten``**.


.. image:: media/erfassung_abgeschlossen.jpg
    :width: 200
