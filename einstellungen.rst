﻿Einstellungen
=============
   
In den Einstellungen können Sie einen Gerätenamen, sowie die
Standardfiliale des Geräts definiert werden. Die ausgewählte Filiale
wird zur Vorbelegung von diversen Filial-Auswahlen benutzt.

Außerdem ist es möglich einen FASHIONCLOUD API-Key zu hinterlegen,
oder diverse Einstellungen an den News-Abonnements, dem Scanner, oder
Ansichten einzustellen.

.. image:: media/Einstellungen_promobile.jpg
   :width: 200


Scanner
-------
   
Neben den Grundeinstellungen zum Verhalten des Scanners wie Ton, Licht und
den akzeptierten Barcodes, können hier Regeln festgelegt werden. Mit
diesen Regeln können Prüfziffern am Anfang, oder Ende eines Barcodes
entfernt werden.

.. image:: media/image13.png
   :width: 200