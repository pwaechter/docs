﻿Einstellungen im Lizenzportal
============================

Anlegen oder importieren der Benutzer
-------------------------------------

1.Melden Sie sich auf https://lizenz.prohandel.de mit Ihren Accountdaten an.

2.Wählen Sie die Benutzerverwaltung aus.

3. Wenn Sie Ihre Personaldaten in der prohandel-Warenwirtschaft korrekt
gepflegt haben, können Sie diese in das Portal importieren.

a. Rufen Sie in der Warenwirtschaft den Personalstamm auf und wählen Sie in der Menüleiste die Schaltfläche **"Lizenzportal"**. Daraufhin wird eine Exportdatei in Ihrem gewählten Ausgabeverzeichnis erstellt.

   .. image:: media/Berechtigungen_Benutzer.jpg
:width: 200

b. Im Lizenzportal (Benutzerverwaltung) wählen Sie  die Schaltfläche **``Import``**.

    .. image:: media/Benutzerverwaltung_Lizenzportal.png
    :width: 200

  
c.	Im erscheinenden Dialog wählen Sie die exportierte Datei aus.

d.  Ist der Import erfolgreich, sehen Sie nun eine Liste Ihres Personals und Vorschläge für Rollen, die aus den prohandel-Berechtigungsgruppen gewonnen werden. Wählen Sie die Benutzer und Rollen aus, die Sie erstellen möchten. Die Rollen werden zunächst leer (d.h. ohne zugewiesene Berechtigungen) angelegt. Die Benutzer sind diesen jedoch schon anhand der Berechtigungsgruppen zugeordnet

e.	Bestätigen Sie den Import.
f.	Sie erhalten nun noch einmal eine Liste der angelegten Benutzer inklusive der temporären Passwörter. Sichern Sie diese Informationen.

4.Alternativ zum Import legen Sie die Benutzer manuell an.

    a.	Klicken Sie in der Benutzerverwaltung auf **``Anlegen``**. 
    b.	Füllen Sie die Maske mit den benötigten Informationen.
    c.	Haben Sie bereits Rollen angelegt, können diese hier ausgewählt werden.
    d.	Bestätigen Sie das Anlegen.
    e.	Sichern Sie das temporäre Passwort für den Benutzer.


.. image:: media/Benutzer_Rollenimport_Lizenzportal.png
:width: 200

Verwalten der Berechtigungen 
----------------------------

Berechtigungen können benutzerübergreifend in Rollen oder für jeden Benutzer individuell eingestellt werden.

1.	Klicken Sie hinter einem Benutzer oder hinter einer Rolle auf das Schlüsselsymbol.
2.	Wählen Sie für jede Berechtigung die gewünschte Einstellung:

    a.	Haben Sie eine Rolle gewählt, setzen Sie einen Haken, wenn die Rolle die Berechtigung beinhalten soll. Benutzer können mehrere Rollen einnehmen. Die verschiedenen Berechtigungen werden dabei additiv kombiniert.
    b.	Haben Sie einen Benutzer gewählt, haben Sie folgende Optionen:

        i.	Aus Rolle erben (Standardeinstellung): Berechtigung für den Benutzer wird aus eingenommenen Rollen gebildet
        ii.	Immer gewähren: Einstellungen aus Rollen werden nicht berücksichtigt. Der Benutzer erhält die Berechtigung in jedem Fall
        iii. Immer verweigern
    
    .. important:: Einstellungen aus Rollen werden nicht berücksichtigt. Der Benutzer erhält die Berechtigung in keinem Fall

3.	Speichern Sie die Änderungen

.. image:: media/Rollenverwaltung_Lizenzportal.png
:width: 200


  