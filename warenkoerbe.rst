﻿Warenkörbe
==========

Warenkörbe bieten die Funktion mehrere Artikel für die weitere
Verarbeitung zu gruppieren.

Anlegen
-------

Um die Arbeit mit den Warenkörben zu beginnen, 
wählen Sie das Menü **``Warenkörbe``** auf der Startseite der promobile-Apps aus. 

Einen Warenkorb können Sie anlegen, indem Sie auf das Plus-Zeichen oben rechts klicken. 

.. image:: media/Warenkorbsuche_.png
   :width: 200


Danach geben Sie Ihre Filiale ein und vergeben Sie den Namen für den Warenkorb.

Abschließend klicken Sie auf **``Warenkorb erstellen``**.


.. image:: media/Warenkorb_erstellen.png
   :width: 200
  

Nun ist der neue Warenkorb erstellt und kann gefüllt werden.         

Warenkorb finden 
==========

Einen passenden Warenkorb können Sie mit Hilfe der Funktion **``Warenkorbsuche``** finden. 

Dafür geben Sie ihren Suchbegriff in die Suchleiste ein. 

Zusätzlich können Sie die Suche nur auf eigene Warenkörben mit dem Klick auf 
**``Beschränkt auf eigene Warenkörbe``** reduzieren. 

.. image:: media/Warenkorbsuche_.png
   :width: 200


Füllen
------

Nachdem Sie einen Warenkorb ausgewählt haben, können Sie ihn füllen, indem Sie:

 - Artikel direkt scannen (Symbol **Scanner** oben im Menü)
 - Artikel manuell eingeben (Symbol Scanner -> Manuell -> EAN-Eingabe)
 - Benutzerdefinierten Artikel im Warenkorb erstellen (genaue Informationen unter 
    :doc: `Benutzerdefinierte Artikel <benutzerdefinierte Artikel>`)
 
  
Alternativ können Sie erstmal in dem Menüpunkt 
**"Artikelsuche"** ihren gewünschten Artikel finden. 
Nachdem ein Artikel ausgewählt wurde, klicken Sie auf das
Symbol **``Einkaufswagen``** oben rechts. Der Artikel wird direkt im Warenkorb 
gespeichert.

.. image:: media/Artikel_hinzufügen.png
   :width: 200


Vorhandene Artikel
~~~~~~~~~~~~~~~~~~

.. image:: media/image39.png
   :width: 200
   
Artikel können über die Ansicht Artikeldetails in einen
Warenkorb gelegt werden. Dazu tippen Sie das Einkaufswagensymbol an,
woraufhin eine Auswahl bestehender Warenkörbe erscheint.

Alternativ ist es möglich an dieser Stelle einen neuen Warenkorb zu
erstellen.

Benutzerdefinierte Artikel
~~~~~~~~~~~~~~~~~~~~~~~~~~

.. image:: media/image37.png
   :width: 200
   
Über die Eingabemaske können Artikel erfasst werden, die
bisher nicht im System verfügbar sind. 

Sie können Sie den Artikelnamen, dessen Preis und Anzahl hinzufügen.
Diese werden im Anschluss in der
Warenwirtschaft weiter bearbeitet.

Details
-------

.. image:: media/image36.png
   :width: 200
   
Die Warenkorbdetails enthalten neben dem benutzerdefinierten
Warenkorbnamen, Informationen zur Erstellung und der Möglichkeit einen
Kunden zuzuweisen eine Liste aller im Warenkorb enthaltenen Artikel.

Die Funktionsschaltflächen im Kopf haben folgende Funktionen

-  Lupe: Hinzufügen eines vorhandenen Artikels mittels Suchfunktion
-  Plus: Hinzufügen eines benutzerdefinierten Artikels
-  Mülleimer: Warenkorb löschen

Für die im Warenkorb enthaltenen Artikel kann außerdem der Preis mittels
Antippen des angezeigten Preises angepasst werden und außerdem die
Anzahl verändert oder der Artikel gelöscht werden. Dazu muss auf dem
Artikel nach links gewischt werden, um das Kontextmenü anzuzeigen.

Über die Schaltfläche **``Warenkorb abschließen``** wird eine kontextsensitive
Auswahl an Verarbeitungsmethoden angezeigt.

Löschen
-------

Um einen Warenkorb zu löschen, wählen Sie
in der Warenkorbsuche den entsprechenden Warenkorb aus und 
klicken Sie auf den Mülleimer oben rechts. 

.. image:: media/Symb.jpg
   :width: 200


Sie werden nochmal gefragt, ob Sie wirklich den Warenkorb löschen wollen. 
Mit **Ok** wird Ihr Befehl ausgeführt.

.. image:: media/Loeschen_WK.jpg
   :width: 200

Abschluss
---------
Mit **``Warenkorb abschließen``** kommen Sie zu zwei Arten von Aufträgen: 

- Kundenauftrag und 

- FASHION CLOUD Auftrag

.. image:: media/image38.png
   :width: 200
   

Aufträge
~~~~~~~~

Die Erstellung von :doc:`Aufträgen <auftrag>` aus einem Warenkorbabschluss ist in einem 
eigenen :doc:`Dokumentationsabschnitt <auftrag>` erläutert.