﻿Kunde
=====

Im Folgenden finden Sie Informationen über den Umgang mit den Kunden.


Anlegen
--------
Um einen neuen Kunden anzulegen, wählen Sie den Menüpunkt **``Kunde``** aus und 
klicken Sie auf den Plus-Symbol oben rechts. 
Sie können direkt alle Ihnen bekannte Kundeninformationen wie 
Name, Adresse, Telefon, sowie Angaben zur Kundenkarte und zum Newsletter machen.
Abschließend speichern Sie alle erfassten Daten.

.. image:: media/NKPlus.jpg
   :width: 200


.. image:: media/NKSpeichern.jpg
   :width: 200

Suche
-----

Der Kundensuche liegt das gleiche Bedienkonzept wie der Artikelsuche
zugrunde, wobei hier keine erweiterten Filter zur Verfügung stehen. 
Geben Sie einfach den Suchbegriff in das Suchfeld ein und klicken auf OK.

.. image:: media/image16.png
   :width: 200


Details
-------

.. image:: media/image17.png
   :width: 200
   
In den Kundendetails erhalten Sie einen umfassenden Überblick
über den ausgewählten Kunden.

Zu Beginn sind grundlegende Informationen wie die Adresse und die
Kontaktinformationen des Kunden aufgeführt. Das Editieren dieser Daten
ist über das Stiftsymbol in der Navigationsleiste möglich.

Im weiteren Verlauf erhalten Sie Informationen über die Anzahl sowie den
aufsummierten Wert von Verkäufen, Gutscheinen, Rechnungen, Auswahlen und
Aufträgen. Insofern der ausgewählte Kunde in einem Bereich Daten
hinterlegt hat, kann der Bereich angetippt werden, um eine entsprechende
Liste auszurufen.

Im Anschluss sind noch Diagramme zu finden, die ein schnelles Urteil
über die meistgekauften Lieferanten, Warengruppen, Größen und Farben des
Kunden ermöglichen. Durch Tippen auf eins der Diagramme können direkt
umfangreichere Informationen angezeigt werden.

Editieren
---------

Das Editieren des Kunden wird über Standardeingabefelder
realisiert.

.. image:: media/image18.png
   :width: 200

.. image:: media/image19.png
   :width: 200
   
Verkäufe
--------

.. image:: media/image20.png
   :width: 200
   
Die Verkäufe eines Kunden können auf dieser Seite mit dem jeweiligen
Verkaufspreis betrachtet werden. Sollte der Artikel reduziert sein, wird
der Ausgangspreis ebenfalls durchgestrichen dargestellt.

Auch ist ein Zugriff auf die Top-Verkaufslisten in den einzelnen
Kategorien mittels der Symbole möglich.

Top-Verkaufslisten
------------------

.. image:: media/image21.png
   :width: 200
   
Die verschiedenen Top-Verkaufslisten sind nach dem gleichen
Prinzip aufgebaut. Mittels der übergeordneten Tabs kann die Topliste
entweder nach Umsatz oder verkaufter Stückzahl angezeigt werden. Die
Einträge sind absteigend sortiert nach dem Anteil am Gesamtumsatz bzw.
dem Anteil an der Gesamtstückzahl. Zur Visualisierung des Wertes enthält
jede Zeile einen farblich abgehobenen Balken dessen Länge den Anteil
widerspiegelt.
   
Gutscheine
----------

.. image:: media/image22.png
   :width: 200
   
Die Seite Gutscheine listet alle noch offenen Gutscheine des Kunden auf
inklusive deren Erstellungs- und Ablaufdatums.

Rechnungen
----------

.. image:: media/image23.png
   :width: 200
   
Die Seite Rechnungen zeigt alle offenen Rechnungen, sowie Gutschriften
des Kunden an. Dabei stellt der kleiner dargestellte Betrag den
Ursprungswert der Rechnung und der größere Betrag, den noch offenen Wert
dar, sodass auch Teilzahlungen identifiziert werden können.

Auswahlen
---------

.. image:: media/image24.png
   :width: 200
   
Die Seite Auswahlen zeigt die aktiven Auswahlen des Kunden inklusive
deren Wert.

Aufträge
--------

.. image:: media/image25.png
   :width: 200
   
Die Seite Aufträge listet die Aufträge des Kunden auf.

Positionen
----------

.. image:: media/image26.png
   :width: 200
   
Für die einzelnen Unterabschnitte (Verkäufe, Gutscheine,
Rechnungen, Auswahlen, Aufträge) können jeweils mit Antippen des
Listeneintrags Detailansichten der Positionen aufgerufen werden.

Hier sind je nach Bereich eine Liste der Positionen oder teilweise auch
weiterführende Informationen abgebildet.

Ein Antippen der Artikel in der Auflistung erlaubt es direkt in die
Artikeldetails zu springen.