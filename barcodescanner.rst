﻿Barcodescanner
==============

Überall, wo Sie das Barcodescanner-Symbol finden, ist es möglich
Barcodes zu scannen. 

Gerätekamera
------------

Beim Tippen auf das Symbol mit dem Barcodescanner öffnet sich ein Dialog, 
der je nach Kontext die Möglichkeit bietet einzelne oder mehrere Artikel 
zu scannen.

Einzel
~~~~~~

.. image:: media/image33.png
   :width: 200
   :align: right 
   
Beim Scannen eines einzelnen Barcodes besteht lediglich die Möglichkeit
das Scannen abzubrechen oder einen Barcode mittels der Kamera zu
scannen, wodurch sich der Dialog automatisch schließt.

.. warning:: Es kann vorkommen, dass das System den gescannten Barcode keinem Artikel oder Kunden zuordnen kann. In diesem Fall erscheint kurz eine entsprechende Meldung auf dem Bildschirm.

.. image:: media/BK.jpg
   :width: 200
 

Während des Scanvorgangs kann man zusätzlich auf einen bestimmten Punkt im Bild
tippen, um die Kamera auf diesen Bereich zu fokussieren.

Mehrfach
~~~~~~~~

.. image:: media/image34.png
   :width: 200
   :align: right 
   
Der Mehrfachscan erweitert den Einzelscan um einige Funktionen, welche
jedoch nicht immer alle bereitstehen müssen. Dieser Dialog schließt sich
nicht automatisch, sondern muss per tippen auf **``Weiter``** oder **``Abbrechen``**
geschlossen werden.

Ein Klick auf das NumPad-Symbol öffnet die manuelle Eingabe.

Der zuletzt gescannte Barcode erscheint immer in der Anzeige. In der
Anzeige ist es möglich mittels wischen nach oben oder unten zuvor
gescannte Barcodes durchzublättern. Barcodes werden i.d.R. unmittelbar
in den gefundenen Typ aufgelöst. Gehört der Barcode zu einem Artikel
wird statt dem Barcode die Artikelkurzinfo dargestellt.

Manuelle Eingabe
~~~~~~~~~~~~~~~~

.. image:: media/image35.png
   :width: 200
   :align: right 
   
Über die manuelle Eingabe können nicht lesbare Barcodes per
Hand eingegeben werden. Die so erfassten Artikel erscheinen als Liste.

Es ist außerdem möglich einen externen Barcodescanner, der mittels
Bluetooth angebunden wird mit dem Handy zu verbinden und anschließend
den Fokus in das Eingabefeld der manuellen Eingabe zu setzen, sodass der
externe Scanner zur Eingabe genutzt werden kann.

Um zum Scannen per Kamera zurückzukehren, klicken Sie auf das
Scanner-Symbol.

Externe Bluetooth-Scanner
-------------------------

Es besteht die Möglichkeit externe Bluetooth-Scanner mit promobile zu verbinden. 
Als Beispiel sehen Sie die Anbindung von Honeywell EDA51.

Honeywell MDE-Geräte
--------------------

Damit der Laser-Scanner des Geräts erfolgreich mit promobile genutzt werden kann,
müssen einige Scaneinstellungen auf dem Gerät angepasst werden. 

Die erforderlichen Schritte sind im Folgenden erläutert.

1. Data Processing

Navigieren Sie in die Geräteeinstellungen

 - Honeywell Settings
 - Scan Settings
 - Internal Scanner
 - Default profile
 - Data Processing Settings

Passen Sie die Einstellungen so an, wie es auf dem Screenshot gezeigt ist. 
In der Regel muss hier lediglich der Zeilenumbruch (\n) als Suffix gesetzt werden. 

.. image:: media/data_processing.png
   :width: 200

2. Symbology

Navigieren Sie in die Geräteeinstellungen 

 - Honeywell Settings 
 - Scan Settings 
 - Internal Scanner 
 - Default profile 
 - Symbology Settings 

Für alle verfügbaren Barcode-Typen muss hier, sofern verfügbar,
die Option **Send check digit** aktiviert werden, 
um zu verhindern, dass Prüfziffern bei Barcodes nicht an die App übergeben werden.

.. image:: media/Symbology.png
   :width: 200
