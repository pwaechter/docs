﻿Aufträge
========

Aufträge können aus dem Abschluss eines Warenkorbes heraus erstellt werden. 
Beim Warenkorbabschluss haben Sie  die Option zwischen dem Kundenauftrag 
und dem FASHION CLOUD Auftrag zu wählen.

Kundenauftrag
-------------

Als Erstes klicken Sie auf den **``Warenkorb abschließen``** unten
und wählen Sie den Kundenauftrag aus. 

.. image:: media/rot.jpg
 :width: 200

.. important:: Der Kunde muss dabei erfasst sein. Sonst erscheint in Rot
 **``Hinterlegter Kunde im Warenkorb erforderlich``**. 

Alle Informationen zum Umgang mit den Kunden finden Sie 
im Kapitel :doc: `Kunde <kunde>`.


Nachdem der gewünschte Kunde hinterlegt wurde, 
können Sie folgende Informationen ergänzen:

- Versandart (Sie haben die Möglichkeit zwischen Abholung, Express Versand und Standard Versand auszuwählen)
- Zahlungsart (Sie können Ihre selbstdefinierte Zahlungarten festlegen wie z.B. Klarna, PayPal, Nachnahme etc.)
- Bestandsfiliale (Sie können definieren aus welcher Filiale der Artikel genommen werden soll).

.. image:: media/Versandart.jpg
   :width: 200

   
.. image:: media/ZA.jpg
   :width: 200

.. image:: media/BF.jpg
   :width: 200


Zusätzlich können Sie auch eine andere Adresse mit dem Setzen des Häckchens 
auf **``Abweichende Rechnungsadresse``**
eingeben. 

.. image:: media/Auftrag.jpg
   :width: 200


Nachdem alle wichtigen Informationen eingegeben wurden, 
schließen Sie den Auftrag ab.

.. image:: media/abweichende_adresse.jpg
   :width: 200


Alle Kundenaufträge erscheinen im Modul „Kundenbestellungen – Auftragsbearbeitung“ in der prohandel-Warenwirtschaft.


Fashioncloud-Auftrag
--------------------

FASHION CLOUD Auftrag ersetzt quasi die OrderWriterApp von FASHION CLOUD.

Die im Warenkorb erfassten Artikel werden über FASHION CLOUD beim Lieferanten bestellt. 

Zusätzlich wird ein Kundenauftrag für die prohandel-Warenwirtschaft erstellt. 

Es ist dabei eine GLN erforderlich. 

**Detaillierte Infos fehlen. Diese Infos werden ergänzt!!!**

.. image:: media/FC.jpg
   :width: 200
  


