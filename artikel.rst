﻿Artikel
========

Im Folgenden finden Sie Informationen über den Umgang mit den Artikeln.

Suche
-----

Die Artikelsuche bietet verschiedene Werkzeuge, um einen
bestimmten Artikel zu finden. Dies kann über eine textbasierte Suche mit
verschiedenen Filtern oder über das Scannen eines Barcodes erreicht
werden.

Haben Sie den Artikel vor sich, empfehlen wir das Scannen des Barcodes
über das Scanner-Symbol in der Suchleiste (weitere Informationen dazu im
Kapitel :doc: `Barcodescanner <barcodescanner>`).

Möchten Sie eine textbasierte Suche durchführen, geben Sie Ihren
Suchbegriff in das Suchfeld ein.


.. image:: media/image14.png
   :width: 200
   
Wenn Sie das Trichtersymbol antippen,
erhalten Sie zusätzlich Zugriff auf einen Warengruppen- und
Lieferantenfilter. 

  
.. image:: media/Lieferantenauswahl.jpg
   :width: 200
 
.. image:: media/Warengruppenauswahl.jpg
   :width: 200

Tippen Sie den Filterkasten an und wählen Sie im
erscheinenden Dialog die gewünschten Warengruppen/Lieferanten aus,
sodass daneben ein Haken erscheint. Haben Sie Ihre Auswahl getroffen,
beenden Sie den Dialog über die **Zurück-Schaltfläche** und die
Einschränkungen werden übernommen.

   
Details
-------

.. image:: media/Art1.jpg
   :width: 200

.. image:: media/Art2.jpg
   :width: 200

   
Die Übersicht über die Artikeldetails ist in **fünf Bereiche** unterteilt:

1. Im oberen Bereich finden Sie die Hauptartikelinformationen
wie Name, Artikelnummer, Fotos des Artikels (wenn verfügbar),
Preis und derzeitiger Bestand.

2. Im zweiten Bereich werden die verfügbaren Farben des Artikels 
und deren Größen angezeigt. Wählen Sie eine abweichende Größe, Farbe oder Filiale aus der Liste aus. 
Der obere Bereich wechselt dabei auf die neue Artikelvariation.

    .. important:: Sollte der Artikel in der anderen Größe oder Farbe nicht verfügbar sein wird das Größen-, bzw. Farbensymbol grau und kann nicht mehr angeklickt werden.

.. image:: media/Grau.jpg
   :width: 200

3. Im dritten Bereich finden Sie die Informationen zu jeder Filiale:
es wird der Bestand, sowie die Anzahl von Auswahlen und 
bereits bestellten Artikeln ausgegeben.

4. Im vierten Bereich erscheinen alle Informationen zu dem von Ihnen ausgewählten
Artikel wie Artikelnummer,EAN, Shop, letzter Wareneingang, Abverkaufsquote etc. angezeigt.

5. Der fünfte Bereich beinhaltet die Fashion Cloud Informationen. 
Der gesamte Bestand des Artikels und die Lieferzeit werden hier angegeben.

.. tip:: Angezeigte Felder können in den Einstellungen angepasst werden.