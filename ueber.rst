﻿Über promobile
==============

promobile, die mobile App für den Einzelhandel, perfektioniert Ihre
Kundenberatung und steigert die Effizienz Ihrer internen Prozesse.

.. image:: media/Unbenannt.png
   :width: 200

Überzeugen Sie Ihre Kunden am POS mit den mobilen Services von promobile:
sobald ein Kunde registriert ist, stellt die App sämtliche Kundendaten für 
Ihr Team bereit – mit persönlichen Vorlieben und der Verkaufshistorie.

Da promobile mit der prohandel Warenwirtschafts-und Kassenlösung verknüpft ist,
lassen sich auch die Artikelbestände in den verfügbaren Farben und Größen
anzeigen – in Echtzeit über alle Filialen hinweg.
Auch Ihre internen Prozesse lassen sich mit promobile effizienter gestalten und
stark vereinfachen: ob es sich um die Inventurerfassung,
Preisänderung, Transfer zu einer anderen Filiale oder die Bestätigung
des Wareneingangs handelt – bietet promobile eine ideale Workflow Erweiterung 
zu den Backend Prozessen in der Warenwirtschaft.

Zudem sind interne oder externe Warenbestellungen für Kunden möglich.

promobile steht Ihnen jederzeit als APP für Android und IOS-Endgeräte zur Verfügung.

**Funktionen von promobile im Überblick**

+ Artikeldaten incl. Bilder / Bestände
+ Kundendaten mit grafischen Auswertungen und Umsatzstatistiken
+ Anlegen und Management von Warenkörben
+ Interne Bestellung aus anderer Filiale
+ Preisänderung
+ Inventur
+ Lieferanten Retoure
+ Wareneingangsprüfung
+ Transfer zu anderen Filialen
+ Flexibles Berechtigungskonzept
+ Personalisierbare Screens
+ Nachbestellung beim Lieferanten

Entwicklermodus
---------------

Um den Entwicklermodus zu aktivieren, wählen Sie den Menü-Punkt 
"Über promobile" und klicken Sie sechs Male hintereinander auf 
das promobile-Logo.

.. image:: media/ueber_promobile.png
   :width: 200


Der Entwicklermodus wird bei der nächsten Anmeldung im Menü erscheinen. 

Der Entwickler bekommt den Zugriff auf das Protokoll,
Geräteinformationen und Startup-Analyse.

.. image:: media/ENTW.jpg
   :width: 200


.. warning:: Im Entwicklermodus werden alle (auch nicht-kritische)  
   Fehler immer in einem Dialog angezeigt und können so den Arbeitsfluss stören.
   Der Entwicklermodus sollte daher im normalen Betrieb deaktiviert werden.

Mit Hilfe des Symbols "Schloss" oben können Sie den Entwicklermodus deaktivieren, 
so dass er aus dem Menü bei der nächsten Anmeldung verschwindet.
 
.. image:: media/ENTWDE.jpg
   :width: 200


