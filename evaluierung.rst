﻿Evaluierung
===========

.. image:: media/image2.png
   :width: 200
   :align: right

Beim erstmaligen Öffnen von promobile, nach der Installation und solange
kein Benutzer angemeldet wurde, erscheint unter der
Schaltfläche **``Anmelden``** die Option des Evaluierungsmodus.
So können Sie die promobile-App für sich testen.

Durch Auswahl der kostenlosen Evaluierung ist es möglich promobile
zunächst in einer vorkonfigurierten Umgebung mit Beispieldaten
auszuprobieren. Es können alle Funktionen der App aufgerufen werden,
jedoch keine Daten geändert werden. 

Wenn Ihre prohandel Installation bereits für den Betrieb mit promobile
eingerichtet wurde, können Sie einfach mit Ihrer Anmeldung fortfahren.

