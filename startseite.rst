﻿Startseite
=============

Menü
----

.. image:: media/image7.png
   :width: 200
   :align: right 
   
Das Menü ist die zentrale Steuerung der App, aus der Sie zu
allen Funktionen navigieren können. Solange Sie sich auf einer
Wurzelseite der App befinden, können Sie das Menü aufrufen, indem Sie
auf das Hamburgermenü (drei horizontale Striche in der linken oberen
Ecke) tippen. Wenn Sie über verschiedene Unterseiten navigieren
erscheint statt dem Hamburgermenü ein nach links zeigender Pfeil, der
genutzt werden kann, um eine Seite zurück zu navigieren. 

Die Anzahl der angezeigten Funktionen kann je nach Umfang der
Berechtigungen der angemeldeten Person variieren.

Nach jedem Start der App wird zunächst die Startseite mit in
verschiedene Bereiche unterteilten Informationen angezeigt. Die
einzelnen Bereiche können per Tippen auf ein Icon oder alternativ eine
Wischbewegung nach links oder rechts durchgeschaltet werden.

.. tip:: Es ist **jederzeit** möglich vom linken Bildschirmrand nach rechts zu wischen,
   um das Menü aufzurufen. Auch, wenn das Menüsymbol nicht und stattdessen der ``Navigation zurück`` 
   Pfeil angezeigt wird.

News
----

.. image:: media/image8.png
   :width: 200
   :align: right
   
Der Newsbereich zeigt Informationen über Softwareaktualisierungen und
Neuigkeiten aus dem prohandel-Umfeld an.

Der Inhalt des Newsbereichs kann bei Bedarf über die Einstellungen auf
beliebige RSS-Feeds angepasst werden.

Warenkörbe
----------

.. image:: media/image9.png
   :width: 200
   :align: right
   
Im Bereich Warenkörbe erscheint eine Übersicht der Warenkörbe
abhängig vom jeweils angemeldeten Verkäufer.

Sollen weitere Warenkörbe oder Warenkörbe anderer Verkäufer angezeigt
werden, kann dies über ein Antippen der Schaltfläche **``Weitere Warenkörbe
anzeigen``** und ein Setzen des entsprechenden Filters gemacht werden.

Suchfunktion
------------

.. image:: media/image10.png
   :width: 200
   :align: right

Die Suchleiste wird unabhängig vom gewählten Bereich angezeigt. Wird
eine Suche ausgelöst, wird automatisch der Suchergebnisbereich
aufgerufen.

Die Daten werden sowohl nach passenden Kunden, als auch Artikeln
durchsucht und bis zu drei Ergebnisse jeder Kategorie angezeigt. Möchten
Sie direkt zu einem Ergebnis springen, brauchen Sie es lediglich
antippen.

Sollte das gewünschte Ergebnis noch nicht zur Auswahl stehen, kann über
ein Antippen der Schaltfläche **``Weitere Kunden anzeigen``** bzw. **``Weitere
Artikel anzeigen``** eine dedizierte (jemandem zugeordnete) Suche nach dieser Kategorie gestartet
werden, bei der der aktuelle Suchbegriff übernommen wird.