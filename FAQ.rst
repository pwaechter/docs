﻿FAQ
====

**1. Werden die Einstellungen auf einem bereits eingerichteten Gerät übernommen?**

Alle Einstellungen werden übernommen.

**2. Kann ich promobile in der Übergangszeit weiter nutzen, 
wenn meine Geräte nicht sofort updaten?**

In der Übergangszeit sind sowohl der alte als auch
der neue Anmeldevorgang gleichzeitig unterstützt. 
Solange Sie eine ältere Version nutzen,
nutzen Sie auch weiterhin die alte Anmeldung. 
Sobald Sie das Update erhalten haben wird die neue verwendet. 
Sie müssen lediglich sicherstellen, dass Sie Ihre Benutzer 
im Lizenzportal angelegt haben.

**3. Welche Daten werden zur Anmeldung verwendet?**

Es wird eine Kombination aus Personalnummer, Firmenname und Passwort zur Anmeldung 
verwendet. Der Firmenname wird, getrennt mit einem Punkt, 
an die Personalnummer angehängt. Dies ist allerdings, 
sobald jemals ein Benutzer an dem Gerät angemeldet wurde, optional.

**4. Wann kann die Anmeldung per PIN verwendet werden?**

Die erste Anmeldung eines Benutzers an einem Gerät muss aus Sicherheitsgründen 
immer mit den vollen Anmeldeinformationen durchgeführt werden. 
Für alle weiteren Anmeldevorgänge kann die PIN genutzt werden.

**5. Warum wird ein temporäres Passwort verwendet?**

Das temporäre Passwort dient lediglich zum Transfer der Anmeldedaten 
zum Benutzer und zur einmaligen Anmeldung. Nach erfolgter Anmeldung muss der Benutzer
das Passwort ändern und in einem weiteren Schritt eine 5-stellige PIN wählen.
