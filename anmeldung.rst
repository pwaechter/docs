﻿Anmeldung
============


   
Nach dem Start der App müssen Sie sich zunächst mit Ihren Login-Daten
authentifizieren, um einen unbefugten Zugriff auf die App zu
unterbinden. 

Geben Sie dazu einfach Ihren Login-Namen und Ihr
persönliches Passwort über die Tastatur ein. Im Anschluss bestätigen Sie
Ihre Eingabe über die Tastatur oder indem Sie auf die 
Schaltfläche **``Anmelden``** drücken.

.. image:: media/image3.png
   :width: 200

Sollte es sich bei der Anmeldung um Ihre erste Anmeldung gehandelt
haben, werden Sie nicht auf die Startseite, sondern zur Vergabe eines
neuen Passwortes weitergeleitet.

Geben Sie dort Ihr altes Passwort ein. Anschließend können Sie ein neues
Passwort vergeben.

    .. Important:: beachten Sie dabei die Beschreibung der Passwortrichtlinien im oberen Teil des Bildschirms.

Durch **``Passwort ändern``** bestätigen Sie die Vergabe Ihres persönlichen
Passworts und es kann zukünftig für die Anmeldung genutzt werden.
Um nicht immer das lange Passwort verwenden zu müssen, können
Sie eine PIN für den Schnellzugriff vergeben.

Bei einer normalen Anmeldung mit den vollen Anmeldeinformationen werden
auf dem Gerät Informationen zur Anmeldung sicher abgelegt. Für eine
Folgeanmeldung mit dem gleichen Account an diesem Gerät ist es daraufhin
möglich die Schnellanmeldung mittels PIN zu nutzen. Dazu geben Sie Ihre
Personalnummer ein, wechseln mittels Enter den Fokus auf die PIN-Eingabe
und geben auch diesen ein. 

Alternativ ist es immer möglich eine normale
Anmeldung mittels Passwort durchzuführen. Tippen Sie dazu auf **``Normale
Anmeldung verwenden``**.


.. image:: media/image4.png
   :width: 200
   

.. image:: media/image5.png
   :width: 200
  

.. image:: media/image6.png
   :width: 200