﻿Transfer / Umlagerung
=====================

.. image:: media/image27.png
   :width: 200
   
Um einen Transfer bzw. eine Umlagerung durchzuführen, wählen
Sie zunächst Quell- und Zielfiliale aus, indem Sie auf die Schaltflächen
tippen und im Dialog die korrekte Filiale wählen.

Anschließend wählen Sie das Scannersymbol aus und scannen die zu
transferierenden Artikel. Haben Sie alle Artikel gescannt tippen Sie im
Dialog auf **``Weiter``**. 
Daraufhin erscheinen alle Artikel in der Kontrollliste.

.. image:: media/tr.jpg
   :width: 200
   

Sie können nun weitere Artikel hinzufügen, die Mengen korrigieren oder
die Buchung abschließen, indem Sie auf die Schaltfläche **``Buchen``** tippen.

Mit dem Klick auf den Mülleimer oben können Sie jederzeit
die erfassten Artikel löschen.

.. image:: media/trwar.jpg
   :width: 200
   

So gebuchte Transfers können in der prohandel-Warenwirtschaft im
Programm ``Transfer / Umlagerung`` verbucht werden. Benutzen Sie dafür
einfach den Button ``Vorerfasste Transfers``.