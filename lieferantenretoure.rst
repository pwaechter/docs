﻿
Lieferantenretoure
=================


Um die Artikel an den Lieferanten zu retournieren,  
wählen Sie den Menü-Punkt „Lieferantenretoure“ im Navigationsbereich aus. 

.. image:: media/lieferantenretoure.png
   :width: 200

Mit Hilfe der Suchfunktion finden Sie Ihre Filiale und den gewünschten Lieferanten. 
Alternativ können Sie dafür die Scanner-Funktion nutzen.
Um den Artikel zu retourenieren, sollte unbedingt der Retouregrund angegeben werden. 
Es können verschiedene **``Retouregründe``** ausgewählt werden:

 - Ware fehlerhaft
 - falscher Artikel geliefert 
 - Webfehler/Materialfehler
 - Starkes Passformproblem
 - Ware stark verschmutzt
 - RV/Knopf defekt etc.

.. image:: media/Retourengründe.png
   :width: 200

Nachdem Sie den Retouregrund ausgewählt haben,
geben Sie die Anzahl der retounierten Teile ein indem Sie die Artikel scannen
und **``BUCHEN``** Sie die Retoure. 

.. image:: media/LR.jpg
   :width: 200


.. warning:: Der Lieferant muss korrekt eingegen werden, sonst erscheint die Meldung,
 dass die Artikel ignoriert werden.

Mit Hilfe des Symbols „Mülleimer“ können Sie alle falsch eingegebenen
Artikel oder die komplette Retoure wieder löschen.


   




