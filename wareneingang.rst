﻿Wareneingang
============
In promobile haben Sie die Möglichkeit den Wareneingang anzulegen oder
zu prüfen.

Anlegen
-------
Um die Wareneingang anzulegen, klicken Sie auf die Funktion **``Anlegen``**. 

.. image:: media/WEAN.jpg
   :width: 200

Mit Hilfe der Suchfunktion können Sie Ihre Filiale und den Lieferanten finden.

.. image:: media/WEANLIF.jpg
   :width: 200


Danach scannen Sie Ihre Lieferscheinnummer.
Scannen Sie die eingetroffenen Artikel oder geben Sie manuell die EAN ein. 
Die Anzahl der Teile wird in der unteren Leiste erscheinen.


.. image:: media/WEANMAN.jpg
   :width: 200

Nachdem alle Eingaben stimmen, klicken Sie auf **``Wischen um zu buchen``**.

.. image:: media/WEANWISH.jpg
   :width: 200
   
Prüfung
-------

.. image:: media/image32.png
   :width: 200
   
Wareneingangsprüfung beinhaltet dieselben Schritte wie das Anlegen
des Wareneingangs.
Um die Wareneingänge zu prüfen, muss eine Filiale, ein Lieferant
und die Lieferscheinnummer eingeben werden. 

Im Anschluss können nach gewohntem Schema alle Artikel gescannt werden.

Über eine Wisch-Geste können Artikel entfernt werden, oder eine
Anpassung ihrer Anzahl durchgeführt werden.

Zum Abschließen der Prüfung muss die **``Buchen``** Schaltfläche betätigt
werden.

Die erfassten Artikel werden im prohandel
Warenwirtschafts-Programm **``Liefermeldungen-EDI``** gegen den erfassten
Lieferschein geprüft.